#include "EPC.h"

#include <chrono>
#include <random>
#include <ctime>

#include <cblas.h>
#include<lapacke.h>

// arXiv:0903.0338  (Table.1)
// Valid from 20-1000 Hz
real PSD_aLIGO(real f) {
	real x = f / 215.0;
	return 1.e-49 * (pow(x,-4.14) - 5.0/(x*x) +  111.0*(1.0 - x*x + 0.5*x*x*x*x) / (1 + 0.5*x*x) );
}
//
// Valid from 10-1000 Hz
real PSD_ET(real f) {
	real x = f / 100.0;
	real tmp = 2.39e-27*pow(x,-15.64) + 0.349*pow(x,-2.145) + 1.76*pow(x,-0.12) + 0.409*pow(x,1.1);
	return 1.e-50 * tmp*tmp;
}
// From LALsuite
real PSD_Virgo(real f) {
	real x  = f/500.;
	real s0 = 10.2e-46;
	return s0*( pow(7.87*x,-4.8) + 6./17./x + 1. + x*x);
}

inline real th2th(real x)   {0.5*M_PI-x;};
inline real phi2phi(real x) { (x>0)? x : 2*M_PI + x;};
inline real psi2psi(real x) {2*M_PI-x;};
#if 1
const int NDet=3;
real Dth[NDet] = {
  th2th(LAL_LHO_4K_DETECTOR_LATITUDE_RAD),
  th2th(LAL_LLO_4K_DETECTOR_LATITUDE_RAD),
  th2th(LAL_VIRGO_DETECTOR_LATITUDE_RAD)
};
real Dphi[NDet] = {
  phi2phi(LAL_LHO_4K_DETECTOR_LONGITUDE_RAD),
  phi2phi(LAL_LLO_4K_DETECTOR_LONGITUDE_RAD),
  phi2phi(LAL_VIRGO_DETECTOR_LONGITUDE_RAD)
};
real Dpsi[NDet] = {
  psi2psi(LAL_LHO_4K_DETECTOR_ARM_X_AZIMUTH_RAD),
  psi2psi(LAL_LLO_4K_DETECTOR_ARM_X_AZIMUTH_RAD),
  psi2psi(LAL_VIRGO_DETECTOR_ARM_X_AZIMUTH_RAD)
};
real (*Dpsd[NDet])(real f) = {PSD_aLIGO, PSD_aLIGO, PSD_Virgo};
#else
const int NDet=1;
real Dth[NDet] = { th2th(LAL_LHO_4K_DETECTOR_LATITUDE_RAD) };
real Dphi[NDet] = { phi2phi(LAL_LHO_4K_DETECTOR_LONGITUDE_RAD) };
real Dpsi[NDet] = { psi2psi(LAL_LHO_4K_DETECTOR_ARM_X_AZIMUTH_RAD) };
real (*Dpsd[NDet])(real f) = {PSD_aLIGO};
#endif


real snr2d[NDet];
void earth2detector(int d, real &th, real &phi, real &psi, const real the, const real phie, const real psie) {
	real thi  = Dth[d];
	real phii = Dphi[d];
	real psii = Dpsi[d];
	
	real cphiei = cos(phie-phii),  sphiei = sin(phie-phii);
	real cphipsii = cos(phi+psii), sphipsii = sin(phi+psii);
	real cth = sin(the)*sin(thi)*cphiei+cos(the)*cos(thi);
	real tph =  (  sin(the)*sin(psii)*cos(thi)*cphiei - cos(the)*sin(thi)*sin(psii) - sin(the)*cos(psii)*sphiei )
	          / ( -sin(the)*cos(psii)*cos(thi)*cphiei + cos(the)*sin(thi)*cos(psii) + sin(the)*sin(psii)*sphiei  ) ;
	real cph =   sin(the)*sin(psie)*sin(thi)*sphipsii - cos(psie)*cphipsii*cphiei 
	           - cphipsii*sin(psie)*cos(the)*sphiei - sphipsii*cos(psie)*cos(thi)*sphiei 
	           + cos(the)*sin(psie)*cos(thi)*sphipsii*cphiei;

	th  = acos(cth);
	//phi = (tph!=tph)? 0.5*M_PI : atan(tph);
	phi = atan(tph);
	psi = acos(cph);
}



///////////////
const real ZERO_ECC = 1.e-2;   // 1e-6 is slow
const real ONE  = 1.0;
const real ZERO = 0.0;

void random_SNR(const char* fname, real SNR, real e0, real Dl0=100, real Mt=20, real eta=0.25) {

    FILE *fout, *fnan, *fdest;
    fout = fopen (fname,"wb");
    fnan = fopen ("nan.bin","wb");

//#define USE_OLDRAND
#ifndef USE_OLDRAND
    //auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    auto seed = 123;
    std::mt19937 myrgen(seed);
    std::uniform_real_distribution<double> rg_th (0.1*M_PI, 0.4*M_PI);
    std::uniform_real_distribution<double> unipi (0.0, M_PI);
    std::uniform_real_distribution<double> uni2pi(0.0, 2*M_PI);
    std::uniform_real_distribution<double> uni(0.9, 1.2);
#else
    srand (123);
#endif
	const int NB=1500;
	const int NP=10001;
	//const int NP=10001;
	const int MAX_V = 8;
	real tc=0, phic=0;

	real Mc = Mt*pow(eta,0.6);
	real FMIN=20.0, FMAX = Flso2(Mt);
	real dF = (FMAX-FMIN)/(NP-1);

	//
	// PSD
	real PSD[NDet][NP];
	real Reh[MAX_V+1][NP];
	real Imh[MAX_V+1][NP];
	real integrand[NP];
	for(int d=0;d<NDet;d++) {
	    #pragma omp parallel for
	    for(int k=0;k<NP;k++) {
		PSD[d][k] = 1.0/Dpsd[d]( FMIN + k*dF );
	    }
	}

	//fprintf(fout, "### e0=%g, Mt=%g eta=%g Dl=%g FMAX=%g\n", e0, Mt, eta, Dl0, FMAX);
	//printf("\n\n#### snr | e0, Mc, eta, cth, be, phi, psi, tc, phic, io,  ln_Dl\n");

	double Sigma[MAX_V][MAX_V] = {0};
	int stat = 9, stat2 = 9;
	int ipiv[MAX_V];
	double work[MAX_V][MAX_V] = {0};
	real ths[NDet], phis[NDet], psis[NDet];

	//
	// Begin MC sampling
	//
	for(int n=0;n<NB; n++)   {
	    fdest = fout;

	// reset FIM for next sample
	double FisherM[MAX_V][MAX_V] = {0};

#ifndef USE_OLDRAND
	real th  = unipi(myrgen); //rg_th(myrgen);
	real phi = uni2pi(myrgen);
	real psi = uni2pi(myrgen);
	real io  = unipi(myrgen);
	real be  = uni2pi(myrgen);      //uni2pi(myrgen);
	real Dl  = Dl0*uni(myrgen);      //uni2pi(myrgen);
#else
	real th  = rand()     * M_PI / RAND_MAX;
	real phi = rand() * 2 * M_PI / RAND_MAX;
	real psi = rand() * 2 * M_PI / RAND_MAX;
	real io  = rand()     * M_PI / RAND_MAX;
	real be  = rand() * 2 * M_PI / RAND_MAX;
	real Dl  = Dl0;
#endif
	for(int d=0; d<NDet; d++) {
	//printf("%g %g %g\n", th,tan(phi),cos(psi));
	    earth2detector(d, ths[d],phis[d],psis[d], th,phi,psi);
	//printf("%g %g %g\n", ths[d],tan(phis[d]),cos(psis[d]));
	}
	
	    // sum over detector network
	    for (int d=0;d<NDet; d++) {
		#pragma omp parallel for
		for(int k=0;k<NP;k++) {

		        EPC epc;
		        real f = FMIN + k*dF;
			
			epc.calall(f, e0, Dl, Mc, eta, ths[d], phis[d], psis[d], io, be, tc,phic);
			integrand[k] = 4.0 * (epc.Reh*epc.Reh + epc.Imh*epc.Imh) * PSD[d][k];
#if 1
			Reh[0][k] = epc.Reh;		Imh[0][k] = epc.Imh;
			Reh[1][k] = epc.Reh_cth;	Imh[1][k] = epc.Imh_cth;  //
			Reh[2][k] = epc.Reh_phi;	Imh[2][k] = epc.Imh_phi;  //
			Reh[3][k] = epc.Reh_e0;		Imh[3][k] = epc.Imh_e0;
			Reh[4][k] = epc.Reh_lMc;  	Imh[4][k] = epc.Imh_lMc;
			Reh[5][k] = epc.Reh_eta;	Imh[5][k] = epc.Imh_eta;
			Reh[6][k] = epc.Reh_cio;    	Imh[6][k] = epc.Imh_cio;   // 000 if io=0
			Reh[7][k] = epc.Reh_tc;		Imh[7][k] = epc.Imh_tc;
			Reh[8][k] = epc.Reh_phic;	Imh[8][k] = epc.Imh_phic;
			//Reh[9][k] = epc.Reh_lDl;	Imh[9][k] = epc.Imh_lDl;   // 000
			//Reh[9][k] = epc.Reh_psi;	Imh[9][k] = epc.Imh_psi;  //
			//Reh[9][k] = epc.Reh_be;		Imh[9][k] = epc.Imh_be;   //
//   Mc = mu^(3/5) M^(2/5)
//   eta = mu / M
//   M = Mc eta^(-3/5)
//   mu = Mc eta^(2/5)
#else
    			Reh[0][k] = epc.Reh;		Imh[0][k] = epc.Imh;
			Reh[1][k] = epc.Reh_cth;	Imh[1][k] = epc.Imh_cth;  //
			//Reh[2][k] = epc.Reh_phi;	Imh[2][k] = epc.Imh_phi;  //
			//Reh[1][k] = epc.Reh_lDl;	Imh[1][k] = epc.Imh_lDl;   // 000
			//Reh[4][k] = epc.Reh_e0;		Imh[4][k] = epc.Imh_e0;
#endif
		}  // end of sampling frequence (and of omp)
		snr2d[d] = simpson(integrand, FMIN, FMAX, NP);
		
		// Calculate Fisher M
		for(int i=0; i<MAX_V; i++)
		for(int j=0; j<=i; j++) {
		    #pragma omp parallel for
		    for(int k=0;k<NP;k++) {
		    	integrand[k] = 4.0 * (Reh[i+1][k]*Reh[j+1][k] + Imh[i+1][k]*Imh[j+1][k] ) * PSD[d][k];
		    }
		    // Keep Fisher to be full matrix for dsymm
		    real tmp = simpson(integrand, FMIN, FMAX, NP);
		    FisherM[i][j] += tmp; 
		    Sigma[i][j] = FisherM[j][i] = FisherM[i][j];
		}
		
	    } // end of detectors
		
#if 0
	printf("== Fisher IM :\n");
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<=i; j++) printf("%11.5g  ", Sigma[i][j]);
		printf("\n");
	}
#endif
	// Calculate Inverse Fisher M : covariance matrix
	// LU decomposition
	stat = LAPACKE_dsytrf(LAPACK_ROW_MAJOR, 'L', MAX_V, *Sigma, MAX_V, ipiv);
	//stat = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, MAX_V, MAX_V, *Sigma, MAX_V, ipiv);
	real det = 1;
	for(int i=0; i<MAX_V; i++) det*=Sigma[i][i];
	
#if 0
	printf("== LU : %g\n", det);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) printf("%11.5g  ", Sigma[i][j]);
		printf("\n");
	}
	for(int i=0; i<MAX_V; i++)  printf("%d  ", ipiv[i]);
	printf("\n");
#endif
	// Inverse
	stat2 = LAPACKE_dsytri2(LAPACK_ROW_MAJOR, 'L', MAX_V, *Sigma, MAX_V, ipiv);
	//stat2 = LAPACKE_dgetri(LAPACK_ROW_MAJOR, MAX_V, *Sigma, MAX_V, ipiv);

#if 0
	printf("== Inverse Fisher IM : %d %d %g\n", stat, stat2, det);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<=i; j++)  printf("%11.5g  ", Sigma[i][j]);
		printf("\n");
	}
#endif

	real inverror = -1;
#if 1
	//printf("== Check Inverse Fisher IM : %d %d \n", stat, stat2);
	cblas_dsymm(CblasRowMajor, CblasLeft, CblasLower, MAX_V, MAX_V, ONE, *Sigma, MAX_V, *FisherM, MAX_V, ZERO, *work, MAX_V);  // C=A*B; B is full matrix
	//cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, MAX_V, MAX_V, MAX_V, ONE, *Sigma, MAX_V, *FisherM, MAX_V, ZERO, *work, MAX_V);

	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<=i; j++) {
			real tmp = std::abs(work[i][j]*work[i][j] - real(i==j));
			if ( tmp > inverror )  inverror = tmp;
		}
		//printf("\n");
	}
#endif
		assert(stat == 0 && stat2 == 0);
		
		real refactor = 1;
		//if (SNR>0) refactor = SNR*SNR / snr2;

		// Network SNR
		real snr2 = 0;
		for(int d=0; d<NDet; d++) { snr2+=snr2d[d]; }

/* 
		1:SNR 2:INVerr 3: det 4:dOmega
		5-12: Mt, eta, Dl, io, be, th, phi, psi
		13-: delta  correlation
*/		
		// check
		real dOmega = 2*M_PI*sqrt( Sigma[1][1]*Sigma[2][2] - Sigma[1][2]*Sigma[1][2] );
		if (dOmega != dOmega)  fdest = fnan;

		/* 
		fprintf(fdest, "%f %g %g %g ", sqrt(snr2 * refactor), sqrt(inverror), det, dOmega );
		fprintf(fdest, "%g %g %g %g %g %g %g %g ", Mt, eta, Dl, io, be, th, phi,psi);
		for(int i=0; i<MAX_V; i++)   fprintf(fdest, "%.5g ", sqrt(Sigma[i][i]) );
		for(int i=0; i<MAX_V; i++)
		for(int j=0; j<i; j++)	    fprintf(fdest, "%.5g ", Sigma[i][j]/sqrt(Sigma[i][i]*Sigma[j][j]) );
		fprintf(fdest, "\n");
		*/
		real snr = sqrt(snr2 * refactor);
		real ier = sqrt(inverror);
		fwrite(&snr, sizeof(real), 1, fdest);
		fwrite(&ier, sizeof(real), 1, fdest);
		fwrite(&det, sizeof(real), 1, fdest);
		fwrite(&dOmega, sizeof(real), 1, fdest);
		fwrite(&Mt, sizeof(real), 1, fdest);
		fwrite(&eta, sizeof(real), 1, fdest);
		fwrite(&Dl, sizeof(real), 1, fdest);
		fwrite(&io, sizeof(real), 1, fdest);
		fwrite(&be, sizeof(real), 1, fdest);
		fwrite(&th, sizeof(real), 1, fdest);
		fwrite(&phi, sizeof(real), 1, fdest);
		fwrite(&psi, sizeof(real), 1, fdest);
		for(int i=0; i<MAX_V; i++)   { snr = sqrt(Sigma[i][i]); fwrite(&snr, sizeof(real), 1, fdest) ; }
		for(int i=0; i<MAX_V; i++)
		for(int j=0; j<i; j++)	 {   
		    snr= Sigma[i][j]/sqrt(Sigma[i][i]*Sigma[j][j]); 
		    fwrite(&snr, sizeof(real), 1, fdest); 
		}
		
#if 1		
		printf("%f %g %g %g ", sqrt(snr2 * refactor), sqrt(inverror), det,dOmega);
		printf("%g %g %g %g %g %g %g %g ", Mt, eta, Dl, io, be, ths, phis,psis);
		for(int i=0; i<MAX_V; i++)   printf("%.5g ", sqrt(Sigma[i][i]) );
		printf("\n");
		//printf("th=%.2f, be=%.2f, phi=%.2f, psi=%.2f, io=%.2f, Dl=%g\n",th/M_PI, be/M_PI, phi/M_PI, psi/M_PI, io/M_PI, Dl);
#endif		
	
	} // end of MC sample
	
	fclose(fout);
	fclose(fnan);
}

void localization(const char* fname, real SNR, real e0, real Dl0=100, real Mt=20, real eta=0.25) {

    FILE *fout, *fnan, *fdest;
    fout = fopen (fname,"wb");
    fnan = fopen ("nan.bin","wb");

	const int NP=10001;
	//const int NP=10001;
	const int MAX_V = 8;
	real tc=0, phic=0;

	real Mc = Mt*pow(eta,0.6);
	real FMIN=20.0, FMAX = Flso2(Mt);
	real dF = (FMAX-FMIN)/(NP-1);

	//
	// PSD
	real iPSD[NDet][NP];
	real Reh[MAX_V+1][NP];
	real Imh[MAX_V+1][NP];
	real integrand[NP];
	for(int d=0;d<NDet;d++) {
	    #pragma omp parallel for
	    for(int k=0;k<NP;k++) {
		iPSD[d][k] = 1.0/Dpsd[d]( FMIN + k*dF );
	    }
	}

	double Sigma[MAX_V][MAX_V] = {0};
	int stat=9, stat2=9;
	int ipiv[MAX_V];
	double work[MAX_V][MAX_V] = {0};
	real ths[NDet], phis[NDet], psis[NDet];
	real th, phi;
	real psi = 0;
	real io  = 0;
	real be  = 0;
	real Dl  = Dl0;

	//
	// Begin All-sky run
	//
	const int  NTH = 15, NPH = NTH*2;
	for(int nt=0;nt<=NTH; nt++)
	for(int np=0;np<=NPH; np++)   {
		
		th  = (real(nt)/NTH)*M_PI;
		phi = (real(np)/NPH)*2*M_PI;
		fdest = fout;

		// reset FIM for next sample
		real FisherM[MAX_V][MAX_V] = {0};

		for(int d=0; d<NDet; d++) {
#if 0
			earth2detector(d, ths[d],phis[d],psis[d], th,phi,psi);
#else
			ths[d] = th;
			phis[d] = phi;
			psis[d] = psi;
#endif
		}

	    // sum over detector network
	    for (int d=0;d<NDet; d++) {
		#pragma omp parallel for
		for(int k=0;k<NP;k++) {

			EPC epc;
			real f = FMIN + k*dF;
			
			epc.calall(f, e0, Dl, Mc, eta, ths[d], phis[d], psis[d], io, be, tc,phic);
			integrand[k] = 4.0 * (epc.Reh*epc.Reh + epc.Imh*epc.Imh) * iPSD[d][k];

			Reh[0][k] = epc.Reh;		Imh[0][k] = epc.Imh;
			Reh[1][k] = epc.Reh_cth;	Imh[1][k] = epc.Imh_cth;  //
			Reh[2][k] = epc.Reh_phi;	Imh[2][k] = epc.Imh_phi;  //
			Reh[3][k] = epc.Reh_e0;		Imh[3][k] = epc.Imh_e0;
			Reh[4][k] = epc.Reh_lMc;  	Imh[4][k] = epc.Imh_lMc;
			Reh[5][k] = epc.Reh_eta;	Imh[5][k] = epc.Imh_eta;
			Reh[6][k] = epc.Reh_cio;    	Imh[6][k] = epc.Imh_cio;   // 000 if io=0
			Reh[7][k] = epc.Reh_tc;		Imh[7][k] = epc.Imh_tc;
			Reh[8][k] = epc.Reh_phic;	Imh[8][k] = epc.Imh_phic;
			//Reh[9][k] = epc.Reh_lDl;	Imh[9][k] = epc.Imh_lDl;   // 000
			//Reh[9][k] = epc.Reh_psi;	Imh[9][k] = epc.Imh_psi;  //
			//Reh[9][k] = epc.Reh_be;		Imh[9][k] = epc.Imh_be;   //
//   Mc = mu^(3/5) M^(2/5)
//   eta = mu / M
//   M = Mc eta^(-3/5)
//   mu = Mc eta^(2/5)

		}  // end of sampling frequence (and of omp)
		snr2d[d] = simpson(integrand, FMIN, FMAX, NP);
		
		// Calculate Fisher M
		for(int i=0; i<MAX_V; i++)
		for(int j=0; j<=i; j++) {
		    #pragma omp parallel for
		    for(int k=0;k<NP;k++) {
		    	integrand[k] = 4.0 * (Reh[i+1][k]*Reh[j+1][k] + Imh[i+1][k]*Imh[j+1][k] ) * iPSD[d][k];
		    }
		    // Keep Fisher to be full matrix for dsymm
		    real tmp = simpson(integrand, FMIN, FMAX, NP);
		    FisherM[i][j] += tmp; 
		    Sigma[i][j] = FisherM[j][i] = FisherM[i][j];
		}
		
	    } // end of detectors
		
#if 0
	printf("== Fisher IM :\n");
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<=i; j++) printf("%11.5g  ", Sigma[i][j]);
		printf("\n");
	}
#endif
	// Calculate Inverse Fisher M : covariance matrix
	// LU decomposition
	stat = LAPACKE_dsytrf(LAPACK_ROW_MAJOR, 'L', MAX_V, *Sigma, MAX_V, ipiv);
	//stat = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, MAX_V, MAX_V, *Sigma, MAX_V, ipiv);
	real det = 1;
	for(int i=0; i<MAX_V; i++) det*=Sigma[i][i];
	
#if 0
	printf("== LU : %g\n", det);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) printf("%11.5g  ", Sigma[i][j]);
		printf("\n");
	}
	for(int i=0; i<MAX_V; i++)  printf("%d  ", ipiv[i]);
	printf("\n");
#endif
	// Inverse
	stat2 = LAPACKE_dsytri2(LAPACK_ROW_MAJOR, 'L', MAX_V, *Sigma, MAX_V, ipiv);
	//stat2 = LAPACKE_dgetri(LAPACK_ROW_MAJOR, MAX_V, *Sigma, MAX_V, ipiv);

#if 0
	printf("== Inverse Fisher IM : %d %d %g\n", stat, stat2, det);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<=i; j++)  printf("%11.5g  ", Sigma[i][j]);
		printf("\n");
	}
#endif
	real inverror = -1;
#if 1
	//printf("== Check Inverse Fisher IM : %d %d \n", stat, stat2);
	cblas_dsymm(CblasRowMajor, CblasLeft, CblasLower, MAX_V, MAX_V, ONE, *Sigma, MAX_V, *FisherM, MAX_V, ZERO, *work, MAX_V);  // C=A*B; B is full matrix
	//cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, MAX_V, MAX_V, MAX_V, ONE, *Sigma, MAX_V, *FisherM, MAX_V, ZERO, *work, MAX_V);

	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<=i; j++) {
			real tmp = std::abs(work[i][j]*work[i][j] - real(i==j));
			if ( tmp > inverror )  inverror = tmp;
		}
		//printf("\n");
	}
#endif
		assert(stat == 0 && stat2 == 0);
		
		real refactor = 1;
		//if (SNR>0) refactor = SNR*SNR / snr2;

		// Network SNR
		real snr2 = 0;
		for(int d=0; d<NDet; d++) { snr2+=snr2d[d]; }

/* 
		1:SNR 2:INVerr 3: det 4:dOmega
		5-12: Mt, eta, Dl, io, be, th, phi, psi
		13-: delta  correlation 
*/		
		// check
		real dOmega = 2*M_PI*sqrt( Sigma[1][1]*Sigma[2][2] - pow(Sigma[1][2],2) );
		//if (dOmega != dOmega)  fdest = fnan;

		/* 
		fprintf(fdest, "%f %g %g %g ", sqrt(snr2 * refactor), sqrt(inverror), det, dOmega );
		fprintf(fdest, "%g %g %g %g %g %g %g %g ", Mt, eta, Dl, io, be, th, phi,psi);
		for(int i=0; i<MAX_V; i++)   fprintf(fdest, "%.5g ", sqrt(Sigma[i][i]) );
		for(int i=0; i<MAX_V; i++)
		for(int j=0; j<i; j++)	    fprintf(fdest, "%.5g ", Sigma[i][j]/sqrt(Sigma[i][i]*Sigma[j][j]) );
		fprintf(fdest, "\n");
		*/
		real snr = sqrt(snr2);
		real ier = sqrt(inverror);
		fwrite(&snr, sizeof(real), 1, fdest);
		fwrite(&ier, sizeof(real), 1, fdest);
		fwrite(&det, sizeof(real), 1, fdest);
		fwrite(&dOmega, sizeof(real), 1, fdest);
		fwrite(&Mt, sizeof(real), 1, fdest);
		fwrite(&eta, sizeof(real), 1, fdest);
		fwrite(&Dl, sizeof(real), 1, fdest);
		fwrite(&io, sizeof(real), 1, fdest);
		fwrite(&be, sizeof(real), 1, fdest);
		fwrite(&th, sizeof(real), 1, fdest);
		fwrite(&phi, sizeof(real), 1, fdest);
		fwrite(&psi, sizeof(real), 1, fdest);
		for(int i=0; i<MAX_V; i++)   { snr = sqrt(Sigma[i][i]); fwrite(&snr, sizeof(real), 1, fdest) ; }
		for(int i=0; i<MAX_V; i++)
		for(int j=0; j<i; j++)	 {
		    snr= Sigma[i][j]/sqrt(Sigma[i][i]*Sigma[j][j]); 
		    fwrite(&snr, sizeof(real), 1, fdest); 
		}
		
#if 0		
		printf("%f %g %g %g ", sqrt(snr2 * refactor), sqrt(inverror), det,dOmega);
		printf("%g %g %g %g %g %g %g %g ", Mt, eta, Dl, io, be, th, phi,psi);
		for(int i=0; i<MAX_V; i++)   printf("%.5g ", sqrt(Sigma[i][i]) );
		printf("\n");
#endif		
	
	} // end of All-sky run
	
	fclose(fout);
	fclose(fnan);
}

int main() {
	//testDer(ZERO_ECC);
	//testDer(0.1);
	//testDer(0.2);
	//CheckDiff();
#if 1
	random_SNR("fisher0.bin",    -1, ZERO_ECC, 100, 10, 0.25);
	random_SNR("fisher1.bin",    -1, 0.1,      100, 10, 0.25);
	random_SNR("fisher2.bin",    -1, 0.2,      100, 10, 0.25);
	random_SNR("fisher3.bin",    -1, 0.3,      100, 10, 0.25);
	random_SNR("fisher4.bin",    -1, 0.4,      100, 10, 0.25);
#endif
//Localization
#if 0
	localization("slocal4d3.bin",    -1, 0.4,      100, 20, 0.25);
	localization("slocal3d3.bin",    -1, 0.3,      100, 20, 0.25);
	localization("slocal2d3.bin",    -1, 0.2,      100, 20, 0.25);
	localization("slocal1d3.bin",    -1, 0.1,      100, 20, 0.25);
	localization("slocal0d3.bin",    -1, ZERO_ECC, 100, 20, 0.25);
#endif

	//random_SNR("fisher0sys.bin", -1, ZERO_ECC);
	//random_SNR("fisher1sys.bin", -1, 0.1);
	//random_SNR("fisher2sys.bin", -1, 0.2);
	//random_SNR("fisher3sys.bin", -1, 0.3);
}

