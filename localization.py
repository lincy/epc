#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
import matplotlib.ticker as mtick
import os

import matplotlib as mpl
from matplotlib.font_manager import FontProperties
mpl.rcParams.update({'font.size': 12})
mpl.rc('font', family='serif')
#mpl.rc('font', family='Microsoft YaHei')
#mpl.rc('axes',edgecolor='#333333')

##mpl.use('Agg')

"""
0:SNR
1:INVErr
2:det
3:Omega
4-11:  Mt, eta, Dl, io, be, th, phi, psi
12-19: delta   (var=8)
20-47  correlation.
"""

VAR=['th','phi','e0','lMc','eta','io','tc','phic']
V0=12
C0=20
NV=len(VAR)
ROWS=48


"""read binary file with pre-unknow line"""
def readbinblock(fn):
    s1 = os.path.getsize(fn)/(8*ROWS)
    f = open(fn, "rb")
    data = np.fromfile(f, np.dtype('d'))
    f.close()
    
    return np.reshape(data, (s1,ROWS))

def readbinaries():
    global d0,d1,d2,d3,d4    
    print("Loading binary....")
    d0 = readbinblock("fisher0.bin")    
    d1 = readbinblock("fisher1.bin")    
    d2 = readbinblock("fisher2.bin")    
    d3 = readbinblock("fisher3.bin")   
    d4 = readbinblock("fisher4.bin")  

def newMollweideAx():
    fig=plt.figure()
    ax = fig.add_subplot(111,projection="mollweide")
    ax.grid(True)
    ax.set_title("3-$\sigma$ $\Delta\Omega$ contour")
    ax.set_position([0,0,1,1])
    ax.xaxis.set_ticklabels([])
    ax.yaxis.set_ticklabels([])
    return ax

def errEclipse(fn, sigma):
    data = readbinblock(fn)
    
    deno = 1. / (data[:,2]*data[:,4]-data[:,3]**2)
    inner_11 =  data[:,4] * deno   ##  <dth^2>
    inner_12 = -data[:,3] * deno   ##  <dth dph>
    inner_22 =  data[:,2] * deno   ##  <dph^2>
    ##sigma = 3
    par = np.linspace(0, 2*np.pi, 1000)
    th = data[:,0]
    ph = data[:,1]
    
    th = np.pi/2 - th
    ph[np.where(ph>np.pi)] = ph[np.where(ph>np.pi)] - 2*np.pi
    tmp = np.sqrt( (inner_11-inner_22)**2 + 4*inner_12*inner_12 )
    lam1 = 0.5*(inner_11 + inner_22 + tmp )   ## eigenvalue
    lam2 = 0.5*(inner_11 + inner_22 - tmp )

    fac1 = inner_12          / np.sqrt(inner_12**2+(lam1-inner_11)**2) * sigma / np.sqrt(lam1)
    fac2 = (lam2 - inner_22) / np.sqrt(inner_12**2+(lam2-inner_22)**2) * sigma / np.sqrt(lam2)
    fac4 = (lam1-inner_11)   / np.sqrt(inner_12**2+(lam1-inner_11)**2) * sigma / np.sqrt(lam1)
    fac5 = inner_12          / np.sqrt(inner_12**2+(lam2-inner_22)**2) * sigma / np.sqrt(lam2)
    
    #plt.figure()
    ax = newMollweideAx()
    
    ## Plot detector location
    ax.plot([d2r(-90,-46),d2r(-119,-24)],[d2r(30,33),d2r(46,27)],'ro')   ## Ligo L1 H1  
    #ax.plot([d2r(10,30),d2r(137,10)],[d2r(43,37),d2r(36,15)],'ro')   ## Virgo KAGRA    
    ax.plot([d2r(10,30)],[d2r(43,37)],'ro')   ## Virgo

    ##
    sin_par = np.sin(par)
    cos_par = np.cos(par)
    for i in range(len(fac1)):
        theta = fac1[i] * cos_par + fac2[i] * sin_par + th[i]
        phi   = fac4[i] * cos_par + fac5[i] * sin_par + ph[i]
        ax.plot(phi,theta,'b',lw=0.8)
    
    #plt.savefig("%s_mono.pdf"%(dat[:-4]), bbox_inches='tight')
    plt.show()
    








def readnetwork(fn):
    s1 = (os.path.getsize(fn)-8)/(8*ROWS)  ## skip 2 int d
    f = open(fn, "rb")

    dim  = np.fromfile(f, np.dtype('i'), count=2)
    data = np.fromfile(f, np.dtype('d'))
    f.close()
    return dim, np.reshape(data, (s1,ROWS))

def readFisherBinary():
    global d0,d1,d2,d3,d4    
    print("Loading binary....")
    d4 = readnetwork("map4.bin")    
    d3 = readnetwork("map3.bin")    
    d2 = readnetwork("map2.bin")    
    d1 = readnetwork("map1.bin")   
    d0 = readnetwork("map0.bin")  

    #fig.savefig('PAD.png', bbox_inches='tight')
def mapFii(fn):
    dim, d = readnetwork(fn)
    tet  = np.reshape(d[:, 9]-0.5*np.pi, dim)
    phi  = np.reshape(d[:,10]-    np.pi, dim)
    data = np.reshape(d[:, 3]*3283, dim)   ##
    myPlotContour(phi,tet, data, 64)

def myPlotContour(phi,tet, data, levs):

    def d2r(deg,min):   return (deg+min/60.)*np.pi/180.  

    #fig, ax = plt.subplots(figsize=(10,6), subplot_kw=dict(projection='mollweide'))
    fig, ax = plt.subplots(figsize=(10,7))
    ax.xaxis.set_ticklabels([])
    contour_plot = ax.contourf( phi.T,tet.T, data.T, levs )   #LEV = np.linspace(data.min(), data.max(),32)
    #contour_plot = ax.contourf( phi.T,tet.T, data.T, locator=mtick.LogLocator())

    ## Plot detector location
    ax.plot([d2r(-90,-46),d2r(-119,-24)],[d2r(30,33),d2r(46,27)],'ro')   ## Ligo L1 H1  
    #ax.plot([d2r(10,30),d2r(137,10)],[d2r(43,37),d2r(36,15)],'ro')   ## Virgo KAGRA    
    ax.plot([d2r(10,30)],[d2r(43,37)],'ro')   ## Virgo

    plt.colorbar(contour_plot, ax=ax, orientation='horizontal', shrink=1, pad=0.03, cmap= mpl.cm.jet)

def detector():
    d = np.loadtxt("detector.dat")
    v = np.loadtxt("LIGO-P1200087-v18-AdV_DESIGN.txt")
    k = np.loadtxt("LIGO-T1600593-v1-KAGRA_Design.txt")
    l = np.loadtxt("LIGO-P1200087-v18-aLIGO_DESIGN.txt")

    #plt.loglog(d[:,0],np.sqrt(d[:,1]),label="aLIGO")
    plt.loglog(v[:,0],v[:,1],label="aVirgo")
    plt.loglog(l[:,0],l[:,1],label="aLIGO")
    #plt.loglog(k[:,0],k[:,1],label="KAGRA")
    plt.xlim([20,2500])
    plt.ylim([1e-24,1e-21])
    plt.legend()

def map(fn):
    dim, d = readnetwork(fn)
    tet  = np.reshape(d[:, 9]-0.5*np.pi, dim)
    phi  = np.reshape(d[:,10]-    np.pi, dim)
    data = np.reshape(d[:, 0], dim)   ##*3282.81
    myPlotContour(phi,tet, data, 64)

def eccEnhanced_dOmega():
    dim, d4 = readnetwork("map4.bin")
    dim, d3 = readnetwork("map3.bin")
    dim, d2 = readnetwork("map2.bin")
    dim, d1 = readnetwork("map1.bin")
    dim, d0 = readnetwork("map0.bin")
    
    tet  = np.reshape(d4[:, 9]-0.5*np.pi, dim)
    phi  = np.reshape(d4[:,10]-    np.pi, dim)

    data = np.reshape(d0[:, 3]/d4[:, 3], dim)   ##
    myPlotContour(phi,tet, data, 64)

    data = np.reshape(d1[:, 3]/d4[:, 3], dim)   ##
    myPlotContour(phi,tet, data, 64)

    data = np.reshape(d2[:, 3]/d4[:, 3], dim)   ##
    myPlotContour(phi,tet, data, 64)

    data = np.reshape(d3[:, 3]/d4[:, 3], dim)   ##
    myPlotContour(phi,tet, data, 64)

def dOmega():
    dim, d4 = readnetwork("map4.bin")
    dim, d3 = readnetwork("map3.bin")
    dim, d2 = readnetwork("map2.bin")
    dim, d1 = readnetwork("map1.bin")
    dim, d0 = readnetwork("map0.bin")
    
    tet  = np.reshape(d4[:, 9]-0.5*np.pi, dim)
    phi  = np.reshape(d4[:,10]-    np.pi, dim)

    data = np.reshape(d4[:, 3]*3283, dim)   ##
    myPlotContour(phi,tet, data, 64)

    data = np.reshape(d3[:, 3]*3283, dim)   ##
    myPlotContour(phi,tet, data, 64)

    data = np.reshape(d2[:, 3]*3283, dim)   ##
    myPlotContour(phi,tet, data, 64)

    data = np.reshape(d0[:, 3]*3283, dim)   ##
    myPlotContour(phi,tet, data, 64)
    
###detector()

#readFisherBinary()

#eccEnhanced_dOmega()
#dOmega()  
#mapFii("map4.bin")    


