#include "helpers.h"

//#define USE_TRAPEZOIDAL
#ifdef USE_TRAPEZOIDAL
real simpson(real* f, real xmin, real xmax, int NPS) {

	real sum=0;
	#pragma omp parallel for reduction(+:sum)
	for (int i=0; i<NPS-1; i++ ) {
		sum += f[i]+f[i+1];
	}
	return sum * (xmax-xmin) / ( (NPS-1) * 2.0);
}
#else
real simpson(real* f, real xmin, real xmax, int NPS) {
	assert(0==(NPS-1)%2);

	real sum=0;
	// FIXME:  omp has buggy behavior here
	//#pragma omp parallel for reduction(+:sum)
	for (int i=0; i<NPS-2; i+=2 ) {
		sum += f[i] + 4*f[i+1] + f[i+2];
	}
	return sum * (xmax-xmin) / ( (NPS-1) * 3.0);
}







#endif

