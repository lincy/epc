#include "EPC.h"

#include <chrono>
#include <random>
#include <ctime>

const real ZERO_ECC = 1e-10;   // 1e-6 is slow
real ONE_  = 1.0;
real ZERO_ = 0.0;

void random_SNR(real SNR, real e0, real Dl0=150, real Mt=20, real eta=0.20) {

#ifndef USE_OLDRAND
    //auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    auto seed = 123;
    std::mt19937 myrgen(seed);
    std::uniform_real_distribution<double> rg_th (0.1*M_PI, 0.4*M_PI);
    std::uniform_real_distribution<double> unipi (0.0, M_PI);
    std::uniform_real_distribution<double> uni2pi(0.0, 2*M_PI);
    std::uniform_real_distribution<double> uni(0.9, 1.1);
#else
    srand (time(NULL));
#endif

	const int NB=5000;
	const int NP=10001;
	const int MAX_V = 6;//8;
	int MAX_V_ = MAX_V;
	real tc=0, phic=0;

	real Mc=Mt*pow(eta,0.6);
	real FMIN=20.0, FMAX = Flso2(Mt);
	real dF = (FMAX-FMIN)/(NP-1);

	//
	// PSD
	real PSD_LIGO[NP];
	real Reh[MAX_V+1][NP];
	real Imh[MAX_V+1][NP];
	real integrand[NP];
	for(int k=0;k<NP;k++) {
		EPC epc;
		real f = FMIN + k*dF;
		PSD_LIGO[k] = epc.PSD_aLIGO(f);
	}

	printf("\n\n#### snr, de0, dMc/Mc, deta/eta, dOmega(deg^2)  ####  e0=%g, Mt=%g eta=%g Dl=%g FMAX=%g\n", e0, Mt, eta, Dl0, FMAX);
	printf("\n\n#### snr | e0, Mc, eta, cth, be, phi, psi, tc, phic, io,  ln_Dl\n");

	//
	// MC sampling
	for(int n=0;n<NB; n++)   {

#ifndef USE_OLDRAND
		real th  = unipi(myrgen); //rg_th(myrgen);
		real phi = uni2pi(myrgen);
		real psi = uni2pi(myrgen);
		real io  = unipi(myrgen);
		real be  = uni2pi(myrgen);      //uni2pi(myrgen);
		real Dl  = Dl0*uni(myrgen);      //uni2pi(myrgen);
#else
		real th  = rand()     * M_PI / RAND_MAX;
		real phi = rand() * 2 * M_PI / RAND_MAX;
		real psi = 0; //rand()     * M_PI / RAND_MAX;
		real io  = 0; //rand()     * M_PI / RAND_MAX;
		real be  = 0; //rand() * 2 * M_PI / RAND_MAX;
#endif

		#pragma omp parallel for
		for(int k=0;k<NP;k++) {
			EPC epc;
			real f = FMIN + k*dF;
			epc.calall(f, e0, Dl, Mc, eta, tc, phic, th, phi, psi, io, be);
			integrand[k] = 4.0 * (epc.Reh*epc.Reh + epc.Imh*epc.Imh) / PSD_LIGO[k];
    			Reh[0][k] = epc.Reh;		Imh[0][k] = epc.Imh;
			Reh[1][k] = epc.Reh_e0;		Imh[1][k] = epc.Imh_e0;
			Reh[2][k] = epc.Reh_Mc;  	Imh[2][k] = epc.Imh_Mc;
			Reh[3][k] = epc.Reh_eta;	Imh[3][k] = epc.Imh_eta;
			Reh[4][k] = epc.Reh_io;    	Imh[4][k] = epc.Imh_io;   // 000 if io=0
			Reh[5][k] = epc.Reh_cth;	Imh[5][k] = epc.Imh_cth;  //
			Reh[6][k] = epc.Reh_phi;	Imh[6][k] = epc.Imh_phi;  //
			//Reh[7][k] = epc.Reh_lDl;	Imh[7][k] = epc.Imh_lDl;   // 000
			//Reh[8][k] = epc.Reh_psi;	Imh[8][k] = epc.Imh_psi;  //
			//Reh[9][k] = epc.Reh_be;		Imh[9][k] = epc.Imh_be;   //
			//Reh[5][k] = epc.Reh_tc;		Imh[5][k] = epc.Imh_tc;
			//Reh[6][k] = epc.Reh_phic;	Imh[6][k] = epc.Imh_phic;

//OK: e, Mc, eta, cth, io, lDl
//OK: e, Mc, eta, io, Dl/phi/be
//			Reh[4][k] = epc.Reh_th;		Imh[4][k] = epc.Im
		}
		real snr2 = (simpson(integrand, FMIN, FMAX, NP));

		// Calculate Fisher M
		real FisherM[MAX_V*MAX_V];
		real Sigma[MAX_V*MAX_V];
		for(int i=0; i<MAX_V; i++) {
			for(int j=i; j<MAX_V; j++) {
				#pragma omp parallel for
				for(int k=0;k<NP;k++) {
					integrand[k] = 4.0 * (Reh[i+1][k]*Reh[j+1][k] + Imh[i+1][k]*Imh[j+1][k] ) / PSD_LIGO[k];
				}
				FisherM[i*MAX_V+j] = Sigma[i*MAX_V+j] = simpson(integrand, FMIN, FMAX, NP);
			}
		}
		for(int i=0; i<MAX_V; i++) {
			for(int j=0; j<i; j++) {
				FisherM[i*MAX_V+j] = Sigma[i*MAX_V+j] = FisherM[j*MAX_V+i];
			}
		}
#if 0
	printf("== Fisher IM : %d %d \n", 0,0);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) {
			printf("%11.5g  ", FisherM[i*MAX_V+j]);
		}
		printf("\n");
	}
#endif
		// Calculate Inverse Fisher M : covariance matrix
		int stat, stat2;
		int ipiv[MAX_V];
		real work[MAX_V*MAX_V];

		// LU decomposition
		FC(dgetrf)(MAX_V_, MAX_V_, Sigma, MAX_V_, ipiv, stat);
		// Inverse
		FC(dgetri)(MAX_V_, Sigma, MAX_V_, ipiv, work, MAX_V_, stat2);

#if 1
	printf("== Inverse Fisher IM : %d %d \n", stat, stat2);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) {
			printf("%11.5g  ", Sigma[i*MAX_V+j]);
		}
		printf("\n");
	}
#endif

	real inverror = -1;
#if 1
	//printf("== Check Inverse Fisher IM : %d %d \n", stat, stat2);
	char NN[]="N";
	FC(dgemm)(NN,NN,MAX_V_,MAX_V_,MAX_V_,ONE_,Sigma,MAX_V_,FisherM,MAX_V_,ZERO_,work,MAX_V_);

	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) {
			real tmp = work[i*MAX_V+j]*work[i*MAX_V+j] - real(i==j);
			if ( tmp > inverror )  inverror = tmp;
			//printf("%11.5g  ", work[i*MAX_V+j]);
		}
		//printf("\n");
	}
	//printf("== Inverse L0 Err = %g\n", sqrt(error));
#endif

		assert(stat == 0 && stat2 == 0);

		real refactor = 1;
		if (SNR>0) refactor = SNR*SNR / snr2;
//		printf("%15g %15g %15g %15g %15g %15g\n", sqrt(snr2 * refactor),
//				sqrt(FisherM[1*MAX_V+1]) / (Mc*M0),

		// check 
		printf("%7.2f (%8.4g) ", sqrt(snr2 * refactor), inverror);
		for(int j=0; j<MAX_V; j++)
		    printf("%13.5g ", sqrt(Sigma[j*MAX_V+j]) );

		real dOmega = 2*M_PI*sqrt( Sigma[6*MAX_V+6]*Sigma[5*MAX_V+5] - pow(Sigma[5*MAX_V+6],2) );
		printf("%5.2f", dOmega);
		
		printf("\n");
		
		//printf("th=%.2f, be=%.2f, phi=%.2f, psi=%.2f, io=%.2f, Dl=%g\n",th/M_PI, be/M_PI, phi/M_PI, psi/M_PI, io/M_PI, Dl);
	
	} // end of MC sample
}

int main() {

	//random_SNR(-1, ZERO_ECC);
	random_SNR(-1, 0.2);
	random_SNR(-1, 0.6);
}
