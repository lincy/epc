#include "EPC.h"

#include <chrono>
#include <random>


const real ZERO_ECC = 1e-13;

void plot() {
	real Dl=100, Mt=20, eta=0.25;
	real FMIN=20., FMAX=Flso2(Mt);

	real e=0.0001;
	real Mc=Mt*pow(eta,0.6);

	const int NP=20001;
	real e0,e1,e2;
	//#pragma omp parallel for
	for(int i=0;i<NP;i++) {
		EPC epc;
		real f = FMIN + (FMAX-FMIN)/NP*i;
		epc.cal(f, 0.0001, Dl,Mc);
		printf("%g %g %g %g\n", f, epc.Reh, epc.Imh, sqrt(epc.Reh*epc.Reh+epc.Imh*epc.Imh));
	}
}

void plotFig1(const char* fname) {

FILE *fout = fopen (fname,"w");
       
	real Dl=100, Mt=20, eta=0.25;
	real FMIN=20., FMAX=Flso2(Mt);
	
	real e;
	real Mc=Mt*pow(eta,0.6);
	real th=0, phi=0,psi=0,io=0,be=0,tc=0,phic=0,thi=0,phii=0,psii=0;
	const int NP=25001;
	real e0,e1,e2, e3, e4;
	//#pragma omp parallel for
	for(int i=0;i<NP;i++) {
		EPC epc;
		real f = FMIN + (FMAX-FMIN)/NP*i;

		epc.cal(f,ZERO_ECC, Dl,Mc, eta);
		e0=sqrt(epc.Reh*epc.Reh + epc.Imh*epc.Imh);
		epc.cal(f, 0.1, Dl,Mc, eta);
		e1=sqrt(epc.Reh*epc.Reh + epc.Imh*epc.Imh);
		epc.cal(f, 0.2, Dl,Mc, eta);
		e2=sqrt(epc.Reh*epc.Reh + epc.Imh*epc.Imh);
		epc.cal(f, 0.3, Dl,Mc, eta);
		e3=sqrt(epc.Reh*epc.Reh + epc.Imh*epc.Imh);
		epc.cal(f, 0.4, Dl,Mc, eta);
		e4=sqrt(epc.Reh*epc.Reh + epc.Imh*epc.Imh);

		fprintf(fout, "%g %g %g %g %g %g\n", f, e0, e1, e2, e3, e4);
	}

}

real EccMisMatch(real mass, real eta) {

	real Dl=100, Mt=mass;
	real tc=0, phic=M_PI*0.25; real th=0.2, phi=0.2, psi=0.2, io=0.2, be=0.2;
	real Mc=Mt*pow(eta,0.6);

	const int NP=100001;
	real FMIN=20., FMAX=1000; //219.;
	real dF = (FMAX-FMIN)/(NP-1);

	real PSD_LIGO[NP];
	real Reh[2][NP];
	real Imh[2][NP];
	real integrand[3][NP];

	const real ZERO_ECC = 1e-11;
	#pragma omp parallel for
	for(int k=0;k<NP;k++) {
		EPC epc;

		real f = FMIN + k*dF;
		PSD_LIGO[k] = epc.PSD_aLIGO(f);
		epc.cal(f, ZERO_ECC, Dl,Mc, eta);
		Reh[0][k] = epc.Reh;
		Imh[0][k] = epc.Imh;
	}

	printf("\n## Mass = %3g\n", mass);
	for(real ecc=0.1; ecc<0.5;ecc+=0.1) {
		#pragma omp parallel for
		for(int k=0;k<NP;k++) {
			EPC epc;

			real f = FMIN + k*dF;
			epc.cal(f, ecc, Dl,Mc, eta);
			Reh[1][k] = epc.Reh;
			Imh[1][k] = epc.Imh;
			integrand[0][k] = 4.0 * (Reh[0][k]*Reh[1][k] + Imh[0][k]*Imh[1][k] ) / PSD_LIGO[k];
			integrand[1][k] = 4.0 * (Reh[0][k]*Reh[0][k] + Imh[0][k]*Imh[0][k] ) / PSD_LIGO[k];
			integrand[2][k] = 4.0 * (Reh[1][k]*Reh[1][k] + Imh[1][k]*Imh[1][k] ) / PSD_LIGO[k];
		}

		real FF = simpson(integrand[0], FMIN, FMAX, NP) / sqrt(simpson(integrand[1], FMIN, FMAX, NP)*simpson(integrand[2], FMIN, FMAX, NP));
		printf("%3g %g\n", ecc, FF);
	}

}

real ____EccFF(real e0, real Mt, real eta) {

	real Dl=100;
	real tc=0, phic=M_PI*0.25; real th=0.2, phi=0.2, psi=0.2, io=0.2, be=0.2;
	real Mc=Mt*pow(eta,0.6);

	const int NP=10001;
	real FMIN=20., FMAX=Flso2(Mt); //219.;
	real dF = (FMAX-FMIN)/(NP-1);

	real PSD_LIGO[NP];
	real Reh[2][NP];
	real Imh[2][NP];
	real integrand[3][NP];

	// signal with
	#pragma omp parallel for
	for(int k=0;k<NP;k++) {
		EPC epc;

		real f = FMIN + k*dF;
		PSD_LIGO[k] = epc.PSD_aLIGO(f);
		epc.cal(f, e0, Dl,Mc, eta);
		Reh[0][k] = epc.Reh;
		Imh[0][k] = epc.Imh;
	}

#if 0
	// random
	int NB=1000;
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 myrgen(seed);

    std::uniform_real_distribution<double> uniMc (Mc* 0.999, Mc*1.001);
    std::uniform_real_distribution<double> unieta(eta*0.999, eta);
    real McM, etaM, FFmax=0.1, EMc, Eeta;

#endif

    real FFmax=1e-320, EMc, Eeta;
    int nMc=201; real Mc0=Mc* 0.999,  Mc1=Mc*1.001,  dMc = (Mc1-Mc0)/(nMc-1);
    int net=51;  real et0=eta* 0.999, et1=eta,      det = (et1-et0)/(net-1);

    printf("Searching over Mc  (%g:%g:%g)\n", Mc0, Mc1, dMc);
    printf("Searching over eta (%g:%g:%g)\n", et0, et1, det);

    for (int i=0; i<nMc; i++)
    for (int j=0; j<net; j++) {

    	real McM  = Mc0 + i*dMc;
    	real etaM = et0 + j*det;

        #pragma omp parallel for
		for(int k=0;k<NP;k++) {
			EPC epc;
			real f = FMIN + k*dF;
			epc.cal(f, ZERO_ECC, Dl, McM, etaM);
			Reh[1][k] = epc.Reh;
			Imh[1][k] = epc.Imh;
			integrand[0][k] = 4.0 * (Reh[0][k]*Reh[1][k] + Imh[0][k]*Imh[1][k] ) / PSD_LIGO[k];
			integrand[1][k] = 4.0 * (Reh[0][k]*Reh[0][k] + Imh[0][k]*Imh[0][k] ) / PSD_LIGO[k];
			integrand[2][k] = 4.0 * (Reh[1][k]*Reh[1][k] + Imh[1][k]*Imh[1][k] ) / PSD_LIGO[k];
		}
		real FF = simpson(integrand[0], FMIN, FMAX, NP) / sqrt(simpson(integrand[1], FMIN, FMAX, NP)*simpson(integrand[2], FMIN, FMAX, NP));
		if (FF>FFmax) {
			FFmax = FF;
			EMc  = (Mc-McM)/Mc*100;
			Eeta = (eta-etaM)/eta*100;
			printf("Maximizing for ... e0= %5g FF= %5f Mt= %6.2e%% eta= %6.2e%% \n", e0, FFmax, EMc, Eeta);
		}
    }
	printf("### e0= %5g FF= %5f Mt= %6.2e% eta= %6.2e%\n", e0, FFmax, EMc, Eeta);
}

real EccFF(real e0, real Mt, real eta) {

	real Dl=100;
	real tc=0, phic=M_PI*0.25; real th=0.2, phi=0.2, psi=0.2, io=0.2, be=0.2;
	real Mc=Mt*pow(eta,0.6);

	const int NP=10001;
	real FMIN=20., FMAX=Flso2(Mt); //219.;
	real dF = (FMAX-FMIN)/(NP-1);

	real PSD_LIGO[NP];
	real Reh[2][NP];
	real Imh[2][NP];
	real integrand[3][NP];

	// signal with
	#pragma omp parallel for
	for(int k=0;k<NP;k++) {
		EPC epc;

		real f = FMIN + k*dF;
		PSD_LIGO[k] = epc.PSD_aLIGO(f);
		epc.cal(f, e0, Dl,Mc, eta);
		Reh[0][k] = epc.Reh;
		Imh[0][k] = epc.Imh;
	}

	// random
	int NB=10000;
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 myrgen(seed);

    int nMc=201; real Mc0=Mc* 0.975,  Mc1=Mc*1.025,  dMc = (Mc1-Mc0)/(nMc-1);
    int net=51;  real et0=eta* 0.98, et1=eta,        det = (et1-et0)/(net-1);

    std::uniform_real_distribution<double> uniMc (Mc0, Mc1);
    std::uniform_real_distribution<double> unieta(et0, eta);
    real McM, etaM, FFmax=0.1, EMc, Eeta;

    printf("Searching over Mc  (%g:%g:%g)\n", Mc0, Mc1, dMc);
    printf("Searching over eta (%g:%g:%g)\n", et0, et1, det);

    for (int i=0; i<NB; i++) {

    	real McM  = uniMc(myrgen);
    	real etaM = unieta(myrgen);

        #pragma omp parallel for
		for(int k=0;k<NP;k++) {
			EPC epc;
			real f = FMIN + k*dF;
			epc.cal(f, ZERO_ECC, Dl, McM, etaM);
			Reh[1][k] = epc.Reh;
			Imh[1][k] = epc.Imh;
			integrand[0][k] = 4.0 * (Reh[0][k]*Reh[1][k] + Imh[0][k]*Imh[1][k] ) / PSD_LIGO[k];
			integrand[1][k] = 4.0 * (Reh[0][k]*Reh[0][k] + Imh[0][k]*Imh[0][k] ) / PSD_LIGO[k];
			integrand[2][k] = 4.0 * (Reh[1][k]*Reh[1][k] + Imh[1][k]*Imh[1][k] ) / PSD_LIGO[k];
		}
		real FF = simpson(integrand[0], FMIN, FMAX, NP) / sqrt(simpson(integrand[1], FMIN, FMAX, NP)*simpson(integrand[2], FMIN, FMAX, NP));
		if (FF>FFmax) {
			FFmax = FF;
			EMc  = (Mc-McM)/Mc*100;
			Eeta = (eta-etaM)/eta*100;
			printf("Maximizing for ... e0= %5g FF= %5f Mt= %6.2e%% eta= %6.2e%% \n", e0, FFmax, EMc, Eeta);
		}
    }
	cout << endl;
    //printf("### e0= %5g FF= %5f Mt= %6.2e%% eta= %6.2e%%\n\n", e0, FFmax, EMc, Eeta);
}

real FisherDepresed(real SNR, real e0, real Mt, real eta=0.25) {

	//=======input
	real Dl=100;
	real tc=0, phic=0;
	real th=0.7923, phi=1.4293, psi=0.7392, io=0.6647, be=1.3754;
	//real th=0., phi=0., psi=0., io=0., be=0.;
	real Mc=Mt*pow(eta,0.6);
	//===============
	printf("------ tc=%f phic=%f th=%f phi=%f psi=%f io=%f be=%f\n", tc,phic,th,phi,psi,io,be);


#if 0
	EPC epc;
	real f=20.0;
	epc.calall(f,0.3,Dl,Mc);
	//epc.cal(f,ZERO_ECC,Dl,Mc, 0.25);
	printf("%g %g %g\n", sqrt(epc.Reh*epc.Reh + epc.Imh*epc.Imh), epc.Reh, epc.Imh );
	printf("%g %g %g\n", sqrt(epc.Reh_e0*epc.Reh_e0 + epc.Imh_e0*epc.Imh_e0),epc.Reh_e0, epc.Imh_e0 );
	printf("%g %g %g\n", sqrt(epc.Reh_lMc*epc.Reh_lMc + epc.Imh_lMc*epc.Imh_lMc),epc.Reh_lMc, epc.Imh_lMc );
	getchar();
#endif

	const int NP=20001;
	const real f0=20;
	real FMIN=f0, FMAX = Flso2(Mt);
	real dF = (FMAX-FMIN)/(NP-1);

	real PSD_LIGO[NP];
	real Reh[6][NP];
	real Imh[6][NP];
	real integrand[NP];

	//printf(" - eta=%g Flso2 = %g\n", eta, FMAX);

	// prepare & SNR
	#pragma omp parallel for
	for(int k=0;k<NP;k++) {
		EPC epc(f0);
		real f = FMIN + k*dF;
		PSD_LIGO[k] = epc.PSD_aLIGO(f);
		//PSD_LIGO[k] = epc.PSD_ET(f);

		epc.calall(f,e0,Dl,Mc,eta,th,phi,psi,io,be);
		Reh[0][k] = epc.Reh;		Imh[0][k] = epc.Imh;
		Reh[1][k] = epc.Reh_e0;		Imh[1][k] = epc.Imh_e0;
		Reh[2][k] = epc.Reh_lMc;  	Imh[2][k] = epc.Imh_lMc;
		Reh[3][k] = epc.Reh_eta;	Imh[3][k] = epc.Imh_eta;
		Reh[4][k] = epc.Reh_tc;		Imh[4][k] = epc.Imh_tc;
		Reh[5][k] = epc.Reh_phic;	Imh[5][k] = epc.Imh_phic;
		//Reh[5][k] = epc.Reh_be;		Imh[5][k] = epc.Imh_be;
		//Reh[6][k] = epc.Reh_phi;	Imh[6][k] = epc.Imh_phi;
		//Reh[7][k] = epc.Reh_psi;	Imh[7][k] = epc.Imh_psi;
		//Reh[0][k] = epc.Reh;	   Imh[0][k] = epc.Imh;
		//Reh[6][k] = epc.Reh_Dl;  Imh[6][k] = epc.Imh_Dl;
		//Reh[7][k] = epc.Reh_io;  Imh[7][k] = epc.Imh_io;
		//Reh[9][k] = epc.Reh_th;  Imh[9][k] = epc.Imh_th;
		integrand[k] = 4.0 * (Reh[0][k]*Reh[0][k] + Imh[0][k]*Imh[0][k] ) / PSD_LIGO[k];
	}
	real snr2 = simpson(integrand, FMIN, FMAX, NP);

	// Calculate Fisher M
	real refactor = 1;
	if (SNR>0.0) refactor = SNR*SNR / snr2;

	int MAX_V = 5;
	real FisherM[MAX_V*MAX_V];
	real FisherI[MAX_V*MAX_V];
	for(int i=0; i<MAX_V; i++) {
		for(int j=i; j<MAX_V; j++) {
			#pragma omp parallel for
			for(int k=0;k<NP;k++) {
				integrand[k] = 4.0 * (Reh[i+1][k]*Reh[j+1][k] + Imh[i+1][k]*Imh[j+1][k] ) / PSD_LIGO[k];
			}
			FisherM[i*MAX_V+j] = FisherI[i*MAX_V+j] = simpson(integrand, FMIN, FMAX, NP) * refactor;
		}
	}
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<i; j++) {
			FisherM[i*MAX_V+j] = FisherI[i*MAX_V+j] = FisherM[j*MAX_V+i];
		}
	}

	// Calculate Inverse Fisher M : covariance matrix
	int stat, stat2;
	int ipiv[MAX_V];
	real work[MAX_V*MAX_V];

	// LU decomposition
	//FC(dgetrf)(MAX_V, MAX_V, FisherM, MAX_V, ipiv, stat);

	// Show determinant
	real det=1;
	for(int i=0; i<MAX_V; i++) det *= FisherM[i*MAX_V+i];
	printf(" ------ e= %4g Mt= %4g eta= %4g det(FIM)= %10g  SNR=%10g   fmax= %g\n", e0, Mt, eta, det, sqrt(snr2), FMAX);

	// Inverse
	//FC(dgetri)(MAX_V, FisherM, MAX_V, ipiv, work, MAX_V, stat2);

#if 0
	printf("== Fisher IM : %d %d \n", stat, stat2);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) {
			printf("%12g  ", FisherI[i*MAX_V+j]);
		}
		printf("\n");
	}
	printf("== Inverse Fisher IM : %d %d \n", stat, stat2);
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) {
			printf("%12g  ", FisherM[i*MAX_V+j]);
		}
		printf("\n");
	}
#endif

	// Diaginal element
	{
		printf("d_e0      = %7.2e\n", sqrt(FisherM[0]) );
		printf("d_Mc/Mc   = %7.2e\n", sqrt(FisherM[1*MAX_V+1]) );
		printf("d_eta/eta = %7.2e\n", sqrt(FisherM[2*MAX_V+2]) / eta);
		printf("d_tc      = %7.2e msec\n", sqrt(FisherM[3*MAX_V+3]) *1000);
		printf("d_phic    = %7.2e\n", sqrt(FisherM[4*MAX_V+4]) );
		//i=5; printf("d_be   = %3d %12g\n", i, sqrt(FisherM[i*MAX_V+i]) );
		//i=6; printf("d_phi  = %3d %12g\n", i, sqrt(FisherM[i*MAX_V+i]) );
		//i=7; printf("d_psi  = %3d %12g\n", i, sqrt(FisherM[i*MAX_V+i]) );
	}

	/*
	// test
	for(int i=0; i<MAX_V; i++) {
		for(int j=0; j<MAX_V; j++) {
			real sum = 0;
			for(int k=0; k<MAX_V; k++) {
				sum+=FisherI[i*MAX_V+k]*FisherM[k*MAX_V+j];
			}
			printf("%d %d %12g\n", i,j,sum);
		}
	} */
}

void testLapack() {

	// Inverse..
	int D=4;

	int stat, stat2;
	int ipiv[D];

	real FisherM[] = { 1,2,3,2,  1,1,0,5,  1,2,1,9,  3,2,9,9 };
	real FisherI[] = { 1,2,3,2,  1,1,0,5,  1,2,1,9,  3,2,9,9 };

	real work[D*D];
	//FC(dgetrf)(D, D, FisherM, D, ipiv, stat);

	// Show determinant
	real det=1;
	for(int i=0; i<D; i++) det *= FisherM[i*D+i];
	printf(" ------ det(FIM) = %15g\n", det);

	//FC(dgetri)(D, FisherM, D, ipiv, work, D, stat2);

	// Calculate Fisher M
	printf("== Inverse Fisher IM : %d %d \n", stat, stat2);
	for(int i=0; i<D; i++) {
		for(int j=0; j<D; j++) {
			printf("%12g  ", FisherM[i*D+j]);
		}
		printf("\n");
	}

	//
	for(int i=0; i<D; i++) {
		printf("%5d %12g\n", i, sqrt(FisherM[i*D+i]*FisherM[i*D+i]) );
	}

	// test
	for(int i=0; i<D; i++) {
		for(int j=0; j<D; j++) {
			real sum = 0;
			for(int k=0; k<D; k++) {
				sum+=FisherI[i*D+k]*FisherM[k*D+j];
			}
			printf("%d %d %12g\n", i,j,sum);
		}
	}

}

void testSimpson() {
	const int NP=11;
	real integrand[NP];
	for(int k=0;k<NP;k++) {

		integrand[k] = 1.;//+sin(2*M_PI*k*(5.*M_PI/(NP-1)));
	}
	printf("%20.15g\n", simpson(integrand, 0, 5, NP));
}

void output_e() {
	real e0;

	real Dl=100, Mt=Mt, eta=0.25;
	real tc=0, phic=0;
	real th=0.7923, phi=1.4293, psi=0.7392, io=0.6647, be=1.3754;

	real Mc=Mt*pow(eta,0.6);

	const int NP=50001;
	real FMIN=20., FMAX=500;
	real dF = (FMAX-FMIN)/(NP-1);

	real ans[10];
//#pragma omp parallel for
	for(int k=0;k<NP;k++) {
		EPC epc;
		real f = FMIN + k*dF;

		for(int i=1; i<=9; i++) {
			//epc.cal(f, 0.1*i, Dl,Mc,eta,tc,phic,th,phi,psi,io,be);
			epc.cal(f, 0.1*i, Dl,Mc);
			ans[i] = epc.e;
		}

		printf("%g %g %g %g %g %g %g %g %g %g\n", f, ans[1], ans[2], ans[3], ans[4], ans[5], ans[6], ans[7], ans[8], ans[9]);
	}
}

void o_M_SNR() {

	const int NP=20001;
	real Dl=100, eta=0.25;
	real integrand[NP];

	for(real Mt=20.; Mt<1000.; Mt*=1.01) {
		real FMIN=20.0, FMAX = Flso2(Mt);
		real dF = (FMAX-FMIN)/(NP-1);

		printf("%5g %10g", Mt, FMAX);
		real Mc=Mt*pow(eta,0.6);
		for(real e0=0.; e0<=0.4; e0+=0.1) {

			#pragma omp parallel for
			for(int k=0;k<NP;k++) {
				EPC epc;
				real f = FMIN + k*dF;
				epc.cal(f, e0+(1.0e-12), Dl, Mc);

				integrand[k] = 4.0 * (epc.Reh*epc.Reh + epc.Imh*epc.Imh) / epc.PSD_aLIGO(f);
			}

			real snr = sqrt(simpson(integrand, FMIN, FMAX, NP));

			printf("%15g ", snr);
		}
		printf("\n");
	}
}


void o_D_SNR() {

	const int NP=10001;
	real eta=0.25;
	real integrand[NP];
	real Mt=20, Mc=Mt*pow(eta,0.6);
	real FMIN=20.0, FMAX = Flso2(Mt);
	real dF = (FMAX-FMIN)/(NP-1);
	//real Dl=100;

	for(real Dl=1000.; Dl<5000; Dl+=50) {

		printf("%5g %10g", Dl, FMAX);
		for(real e0=0.; e0<=0.4; e0+=0.1) {

			#pragma omp parallel for
			for(int k=0;k<NP;k++) {
				EPC epc;
				real f = FMIN + k*dF;
				epc.cal(f, e0+(1.0e-14), Dl, Mc);

				integrand[k] = 4.0 * (epc.Reh*epc.Reh + epc.Imh*epc.Imh) / epc.PSD_aLIGO(f);
			}

			real snr = sqrt(simpson(integrand, FMIN, FMAX, NP));

			printf("%15g ", snr);
		}
		printf("\n");
	}
}

void check_SNR() {

	const int NP=10001;
	real Dl=100, eta=0.25, Mt=20;
	real tc=0, phic=0;
	real th=0, phi=0, psi=0, io=0, be=0;


	real Mc=Mt*pow(eta,0.6);
	real FMIN=20.0, FMAX = Flso2(Mt);
	real dF = (FMAX-FMIN)/(NP-1);


	real integrand[NP];

	real *var = &io;
	for(*var=0.; *var<2*M_PI; *var+=0.1) {

		printf("%5g ", *var);

		for(real e0=0.; e0<=0.4; e0+=0.1) {

			#pragma omp parallel for
			for(int k=0;k<NP;k++) {
				EPC epc;
				real f = FMIN + k*dF;
				epc.cal(f,e0+ZERO_ECC,Dl,Mc,eta,th,phi,psi,io,be);
				integrand[k] = 4.0 * (epc.Reh*epc.Reh + epc.Imh*epc.Imh) / epc.PSD_aLIGO(f);
			}
			real snr = sqrt(simpson(integrand, FMIN, FMAX, NP));

			printf("%15g ", snr);
		}
		printf("\n");
	}
}




int main() {


	//plot();

	plotFig1("fig1.dat");  // produce Fig1


	//EccFF(0.1, 20, 0.25);
	//EccFF(0.2, 20, 0.25);
	//EccFF(0.3, 20, 0.25);
	//EccFF(0.4, 20, 0.25);

	//testSimpson();

	//EccMisMatch(2.8, 0.25);
	//EccMisMatch(11.4, 0.1077);
	//EccMisMatch(20.0, 0.25);
	//testLapack();

	//output_e();
	//o_M_SNR();
	//o_D_SNR();

	//check_SNR();

/*
	#if 0
	Fisher(10, ZERO_ECC, 20);
	Fisher(10, 0.1, 20);
	Fisher(10, 0.2, 20);
	Fisher(10, 0.3, 20);
	Fisher(10, 0.4, 20);
#elif 0
	Fisher(-1, ZERO_ECC, 20);
	Fisher(-1, 0.1, 20);
	Fisher(-1, 0.2, 20);
	Fisher(-1, 0.3, 20);
	Fisher(-1, 0.4, 20);
#endif
*/
}


