#include "helpers.h"
#include <fstream>
#include <cmath>
#include <cstdlib>
using std::ifstream;
#include <gsl/gsl_errno.h>
#include <gsl/gsl_spline.h>

const int LEN=3000;

void loadpsd(const char* name, const real inf[], real outpsd[], const int NP) {
    ifstream infile;
    infile.open(name);
    
    real f[LEN], psd[LEN];
    int count=0;
    while(!infile.eof()) { 
        infile >> f[count] >> psd[count];
        count++;
        if (count>=LEN) break;
    }
    infile.close();
    
    //
    //  interpolation
    //
    gsl_interp_accel *acc = gsl_interp_accel_alloc ();
    gsl_spline *spline = gsl_spline_alloc (gsl_interp_cspline, count);
    gsl_spline_init (spline, f, psd, count);
#include <cmath>
    for (int i = 0; i < NP; i++) {
        outpsd[i] = gsl_spline_eval (spline, inf[i], acc);
	// LIGO analytical form for test...
	//real x = inf[i] / 215.0;
	//outpsd[i] = 1.e-49 * (pow(x,-4.14) - 5.0/(x*x) +  111.0*(1.0 - x*x + 0.5*x*x*x*x) / (1 + 0.5*x*x) );
    }
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
}


#ifdef TEST
int main() {
    
    const int NP=10001;
    real FMIN=20, FMAX=1500;
    real dF=(FMAX-FMIN)/(NP-1); 
    real f[NP], PSD[NP] = {0};
    for (int i = 0; i < NP; i++) {
	f[i] = FMIN+i*dF;
    }
    
    interpolate(PSD_AVIRGO, f, PSD, NP);

}
#endif