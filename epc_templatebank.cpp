#include "EPC.h"

#include <chrono>
#include <random>
#include <ctime> 

const real ZERO_ECC = 1e-13;

void template_ff(real e0, real M_max=70, real Dl=100) {

#ifndef USE_OLDRAND
    auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
    std::mt19937 myrgen(seed);
    std::uniform_real_distribution<double> uni_mt(4.0, M_max);
    std::uniform_real_distribution<double> uni_eta(0, 0.25);
#else
    //srand (time(NULL));
#endif

	const int NB=1200;
	const int NP=10001;

	real tc=0, phic=0;
	real th=0, phi=0, psi=0, io=0, be=0;

	//
	// PSD
	real Reh[2][NP];
	real Imh[2][NP];
	real integrand[3][NP];
	real PSD_LIGO[NP];

	printf("\n\n#### m1, m2, ff ;  e0=%g, Mt=%g Dl=%g\n", e0, M_max, Dl);

	//
	// MC sampling
	for(int n=0;n<NB; n++)   {

#ifndef USE_OLDRAND
		real mt  =  uni_mt(myrgen);
		real eta =  uni_eta(myrgen);
#else
		//real th  = rand()     * M_PI / RAND_MAX;
		//real phi = rand() * 2 * M_PI / RAND_MAX;
#endif

		real FMIN=20.0, FMAX = Flso2(mt);
		real dF = (FMAX-FMIN)/(NP-1);
		if (FMAX < FMIN) { printf("Fmax < Fmin !!\n"); }

		real Mc=mt*pow(eta,0.6);

		#pragma omp parallel for
		for(int k=0;k<NP;k++) {
			EPC epc;
			real f = FMIN + k*dF;
			epc.cal(f, ZERO_ECC, Dl, Mc, eta, tc, phic, th, phi, psi, io, be);
			Reh[0][k] = epc.Reh;		Imh[0][k] = epc.Imh;

			epc.cal(f,       e0, Dl, Mc, eta, tc, phic, th, phi, psi, io, be);
			Reh[1][k] = epc.Reh;		Imh[1][k] = epc.Imh;
			PSD_LIGO[k] = 1.0/epc.PSD_aLIGO(f);

			integrand[0][k] = 4.0 * (Reh[0][k]*Reh[1][k] + Imh[0][k]*Imh[1][k] ) * PSD_LIGO[k];
			integrand[1][k] = 4.0 * (Reh[0][k]*Reh[0][k] + Imh[0][k]*Imh[0][k] ) * PSD_LIGO[k];
			integrand[2][k] = 4.0 * (Reh[1][k]*Reh[1][k] + Imh[1][k]*Imh[1][k] ) * PSD_LIGO[k];
		}
		real FF = simpson(integrand[0], FMIN, FMAX, NP) / sqrt(simpson(integrand[1], FMIN, FMAX, NP)*simpson(integrand[2], FMIN, FMAX, NP));
		real m1= 0.5*mt*( 1 + sqrt(1.-4*eta));
		real m2= 0.5*mt*( 1 - sqrt(1.-4*eta));
		printf("%15g %15g %15g\n", m1, m2, FF);
	}
}


int main() {

	template_ff(0.1);
	template_ff(0.2);
	template_ff(0.3);
	template_ff(0.4);
}
