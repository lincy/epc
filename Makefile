include Makefile.inc

OBJECTS=EPC_cal.o helpers.o loadpsd.o
## dgetri.o dgetrf.o


run:
		export OMP_NUM_THREADS=20; ./epc

all: epc_network
loadpsd: loadpsd.o
		$(CXX) $(OPT) $@.o -o $@ $(LIBS)

epc_network: epc_network.o $(OBJECTS)
		$(CXX) $(OPT) $@.o $(OBJECTS) -o $@ $(LIBS)

epc_random: epc_random.o $(OBJECTS)
		$(CXX) $(OPT) $@.o $(OBJECTS) -o $@ $(LIBS)



epc_templatebank: epc_templatebank.o $(OBJECTS)
		$(CXX) $(OPT) $@.o $(OBJECTS) -o $@ $(LIBS)

epc: epc.o $(OBJECTS)
		$(CXX) $(OPT) $@.o $(OBJECTS) -o $@ $(LIBS)
		
		