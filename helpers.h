#ifndef _HELPERS_HH_
#define _HELPERS_HH_

#define LAL_LHO_4K_DETECTOR_LONGITUDE_RAD      -2.08405676917   /**< LHO_4k vertex longitude (rad) */
#define LAL_LHO_4K_DETECTOR_LATITUDE_RAD        0.81079526383   /**< LHO_4k vertex latitude (rad) */
#define LAL_LHO_4K_DETECTOR_ARM_X_AZIMUTH_RAD   5.65487724844   /**< LHO_4k x arm azimuth (rad) */
#define LAL_LLO_4K_DETECTOR_LONGITUDE_RAD      -1.58430937078   /**< LLO_4k vertex longitude (rad) */
#define LAL_LLO_4K_DETECTOR_LATITUDE_RAD        0.53342313506   /**< LLO_4k vertex latitude (rad) */
#define LAL_LLO_4K_DETECTOR_ARM_X_AZIMUTH_RAD   4.40317772346   /**< LLO_4k x arm azimuth (rad) */
#define LAL_VIRGO_DETECTOR_LONGITUDE_RAD        0.18333805213   /**< VIRGO vertex longitude (rad) */
#define LAL_VIRGO_DETECTOR_LATITUDE_RAD         0.76151183984   /**< VIRGO vertex latitude (rad) */
#define LAL_VIRGO_DETECTOR_ARM_X_AZIMUTH_RAD    0.33916285222   /**< VIRGO x arm azimuth (rad) */

#define PSD_AVIRGO "LIGO-P1200087-v18-AdV_DESIGN.txt"
#define PSD_ALIGO  "LIGO-P1200087-v18-aLIGO_DESIGN.txt"
#define PSD_KAGRA  "LIGO-T1600593-v1-KAGRA_Design.txt"

#include <assert.h>
typedef double real;

const real G_SI = 6.67408e-11;  // m^3 kg^-1 s^-2
const real C_SI = 299792458.0;
const real M0  = 1.99e30 * G_SI / (C_SI*C_SI*C_SI);   // 4.93 us
const real Mpc = 3.08567758e22 / C_SI;       //

real simpson(real* f, real xmin, real xmax, int NPS);

void loadpsd(const char* name, const real inf[], real outpsd[], const int NP);

#endif

