#include "EPC.h"

inline real Power(real x, real p) {return pow(x,p); }
inline real Sin(real x) {return sin(x); }
inline real Cos(real x) {return cos(x); }
inline real Sqrt(real x) {return sqrt(x); }
inline real Log(real x) {return log(x); }
inline real ArcTan(real x) {return atan(x); }
inline real Step(real x) {return (x>=0? 1:0); }
template <typename T> int Sign(T val) {
    return (T(0) < val) - (val < T(0));
};
const static real Pi = M_PI;
const static real EulerGamma = 0.5772156649015328606;


void EPC::cal(real f,real e0,real Dl,real Mc,real eta,real th,real phi,real psi,real io,real be,real tc,real phic,real thi,real phii,real psii) {
	Mc*=M0;
	Dl*=Mpc;
	#include "epc_nb.h"
	//#include "tf2_nb.h"
}
void EPC::calall(real f,real e0,real Dl,real Mc,real eta,real th,real phi,real psi,real io,real be,real tc,real phic,real thi,real phii,real psii) {
	Mc*=M0;
	Dl*=Mpc;
	#include "epc_nb_deriv.h"
}
