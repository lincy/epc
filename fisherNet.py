#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
import os

import matplotlib as mpl
from matplotlib.font_manager import FontProperties
mpl.rcParams.update({'font.size': 18})
mpl.rc('font', family='serif')
#mpl.rc('font', family='Microsoft YaHei')
#mpl.rc('axes',edgecolor='#333333')

##mpl.use('Agg')

"""
0:SNR
1:INVErr
2:det
3:Omega
4-11:  Mt, eta, Dl, io, be, th, phi, psi
12-19: delta   (var=8)
20-47  correlation.
"""

VAR=['th','phi','e0','lMc','eta','io','tc','phic']
V0=12
C0=20
NV=len(VAR)
ROWS=48

def readtext():
    global d0,d1,d2,d3,d4    
    print("Loading....")
    d1 = np.loadtxt("fisher1.dat", skiprows=1)
    d2 = np.loadtxt("fisher2.dat", skiprows=1)
    d3 = np.loadtxt("fisher3.dat", skiprows=1)
    d4 = np.loadtxt("fisher4.dat", skiprows=1)
    d0 = np.loadtxt("fisher0.dat", skiprows=1)

"""read binary file with pre-unknow line"""
def readbinblock(fn):
    s1 = os.path.getsize(fn)/(8*ROWS)
    f = open(fn, "rb")
    data = np.fromfile(f, np.dtype('d'))
    f.close()
    return np.reshape(data, (s1,ROWS))

def readbinaries():
    global d0,d1,d2,d3,d4    
    print("Loading binary....")
    d1 = readbinblock("d1/fisher3.bin")    
    d3 = readbinblock("d3/fisher3.bin")    

"""core plot task """
def pltmydata(var, **params):
    plt.figure(figsize=(10,7))
    plt.hist(d1[:,var], **params, color='black', label="D1")
    plt.hist(d3[:,var], **params, color='r',     label="D3")
    plt.ylabel("# samples")   

def plotSNR():
    ### SNR
    var=0    
    params= {"bins":64,"alpha":1,"histtype":'step'}
    pltmydata(var, **params)
    plt.xlabel("SNR")   
    plt.savefig('epc_snr.png', bbox_inches='tight')

def plotdOmega():
    var=3
    MIN = min( min(d0[:,var]), min(d1[:,var]), min(d2[:,var]), min(d3[:,var]), min(d4[:,var]) )
    MAX = max( max(d0[:,var]), max(d1[:,var]), max(d2[:,var]), max(d3[:,var]), max(d4[:,var]) )
    params= {"bins":10 ** np.linspace(np.log10(MIN), np.log10(MAX), 128),"alpha":1,"histtype":'step'}

    pltmydata(var, **params)
    plt.gca().set_xscale("log")
    plt.xlabel("d$\Omega$")      
    plt.savefig('epc_dOmega.png', bbox_inches='tight')

def plotDet():
    var=2
    MIN = min( min(d0[:,var]), min(d1[:,var]), min(d2[:,var]), min(d3[:,var]), min(d4[:,var]) )
    MAX = max( max(d0[:,var]), max(d1[:,var]), max(d2[:,var]), max(d3[:,var]), max(d4[:,var]) )
    params= {"bins":10 ** np.linspace(np.log10(MIN), np.log10(MAX), 128),"alpha":1,"histtype":'step'}
    pltmydata(var, **params)
    plt.gca().set_xscale("log")
    plt.xlabel("Det")      
    plt.savefig('epc_det.png', bbox_inches='tight')

def plotINVErr():
    var=1
    MIN = min( min(d0[:,var]), min(d1[:,var]), min(d2[:,var]), min(d3[:,var]), min(d4[:,var]) )
    MAX = max( max(d0[:,var]), max(d1[:,var]), max(d2[:,var]), max(d3[:,var]), max(d4[:,var]) )
    params= {"bins":10 ** np.linspace(np.log10(MIN), np.log10(MAX), 128),"alpha":1,"histtype":'step'}

    pltmydata(var, **params)
    plt.gca().set_xscale("log")
    plt.legend()      
    plt.xlabel("INV Error")      
    plt.savefig('epc_inverr.png', bbox_inches='tight')

def plotFii():
    for var in range(V0,C0):
      MIN = min( min(d0[:,var]), min(d1[:,var]), min(d2[:,var]), min(d3[:,var]), min(d4[:,var]) )
      MAX = max( max(d0[:,var]), max(d1[:,var]), max(d2[:,var]), max(d3[:,var]), max(d4[:,var]) )
      params= {"bins":10 ** np.linspace(np.log10(MIN), np.log10(MAX), 128),"alpha":1,"histtype":'step'}
    
      pltmydata(var, **params)
      plt.gca().set_xscale("log")
      plt.legend()      
      plt.xlabel(VAR[var-V0])      
      plt.savefig('epc_'+VAR[var-V0]+'.png', bbox_inches='tight')

def plotFij():
    co=C0
    for i in range(0,NV):
      for j in range(0,i):
        params= {"bins":np.linspace(-1,1,41),"alpha":1,"histtype":'step'}
        
        pltmydata(co, **params)
        plt.xlabel(VAR[i]+'-'+VAR[j])      
        co = co+1    

def plotFijGrid(nv):
    fig = plt.figure(figsize=(50,50))
    plt.clf()
    
    params= {"bins":np.linspace(-1,1,101),"alpha":1,"histtype":'step'}
    co=C0
    for i in range(0,nv):
      for j in range(0,i+1):
        ax = plt.subplot2grid((nv,nv), (i, j))
        if i==j: 
            ax.text(0.1,0.1, VAR[i], fontsize=20, transform=ax.transAxes)
            ax.axis('off')
            continue
        ax.set_xlim([-1,1])
        ax.hist(d1[:,co], **params, color='black', label="D1")
        ax.hist(d3[:,co], **params, color='r',     label="D3")
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        
        co = co+1
    plt.savefig('epc_cor.png', bbox_inches='tight')
    plt.show()

#readtext()
readbinaries()

plotSNR()
plotINVErr()
plotDet()
plotdOmega()
plotFii()
plotFijGrid(NV)
