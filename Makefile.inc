CXX=g++
CFLAG=-std=c++11 -Wno-write-strings

FC=gfortran

INCLUDE=-I/home/lincy/local/include
LIBS=-L/usr/lib64 -lopenblas -L/home/lincy/local/lib -Wl,-rpath='/home/lincy/local/lib' -lgsl
#-lopenblas -L/home/lincy/local/lib -Wl,-rpath='/home/lincy/local/lib'



OPT=-O2 -fopenmp
#OPT=-O2 -fp-model precise
#-unroll-aggressive -parallel -opt-prefetch


.SUFFIXES : .cpp .o .f90 .f

.cpp.o:
		$(CXX) $(OPT) $(CFLAG) $(INCLUDE) $< -c -o $@
.f90.o:
		$(FC) $(OPT) $< -c -o $@
.f.o:
		$(FC) $(OPT) $< -c -o $@

default: all

clean:
		rm *.o -f

