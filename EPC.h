#ifndef _EPC_H_
#define _EPC_H_

#include <iostream>
using std::cout;
using std::endl;
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "helpers.h"

typedef double real;

// GW frequency in the LSO
inline real Flso2(real Mt) { return 1.0 / ( pow(6.0,1.5)*M_PI*Mt*M0 ); }

#define LAL_LHO_4K_DETECTOR_LONGITUDE_RAD      -2.08405676917   /**< LHO_4k vertex longitude (rad) */
#define LAL_LHO_4K_DETECTOR_LATITUDE_RAD        0.81079526383   /**< LHO_4k vertex latitude (rad) */
#define LAL_LHO_4K_DETECTOR_ARM_X_AZIMUTH_RAD   5.65487724844   /**< LHO_4k x arm azimuth (rad) */
#define LAL_LLO_4K_DETECTOR_LONGITUDE_RAD      -1.58430937078   /**< LLO_4k vertex longitude (rad) */
#define LAL_LLO_4K_DETECTOR_LATITUDE_RAD        0.53342313506   /**< LLO_4k vertex latitude (rad) */
#define LAL_LLO_4K_DETECTOR_ARM_X_AZIMUTH_RAD   4.40317772346   /**< LLO_4k x arm azimuth (rad) */
#define LAL_VIRGO_DETECTOR_LONGITUDE_RAD        0.18333805213   /**< VIRGO vertex longitude (rad) */
#define LAL_VIRGO_DETECTOR_LATITUDE_RAD         0.76151183984   /**< VIRGO vertex latitude (rad) */
#define LAL_VIRGO_DETECTOR_ARM_X_AZIMUTH_RAD    0.33916285222   /**< VIRGO x arm azimuth (rad) */

/*
struct Det {
	
	real f0;
	real th, psi, psi;
	
	real cphiei = cos(phie-phii);
	real sphiei = sin(phie-phii);
	real cphipsii = cos(phi+psii);
	real sphipsii = sin(phi+psii);
	real cth = sin(the)*sin(thi)*cphiei+cos(the)*cos(thi);
	real tph =  (  sin(the)*sin(psii)*cos(thi)*cphiei - cos(the)*sin(thi)*sin(psii) - sin(the)*cos(psii)*sphiei ) 
	          / ( -sin(the)*cos(psii)*cos(thi)*cphiei + cos(the)*sin(thi)*cos(psii) + sin(the)*sin(psii)*sphiei  ) ;
	real cph =   sin(the)*sin(psie)*sin(thi)*sphipsii - cphipsii*cphiei - cphipsii*sin(psie)*cos(the)*sphiei
	           - sphipsii*cos(psie)*cos(thi)*sphiei + cos(the)*sin(psie)*cos(thi)*sphipsii*cphiei;
}
*/

class EPC {
public:
	real f0;

	real e;
	real Reh, Imh;
	real Reh_e0,  Imh_e0;
	real Reh_cth,  Imh_cth;
	real Reh_phi, Imh_phi;
	real Reh_lMc, Imh_lMc;
	real Reh_eta, Imh_eta;
	real Reh_cio,  Imh_cio;
	real Reh_be,  Imh_be;
	real Reh_lDl, Imh_lDl;
	real Reh_psi, Imh_psi;

	real Reh_tc,  Imh_tc;
	real Reh_phic,  Imh_phic;
    
	void cal   (real f,real e0,real Dl,real Mc,real eta=0.25,real th=0,real phi=0,real psi=0,real io=0,real be=0,real tc=0,real phic=0, real thi=0,real phii=0,real psii=0);
	void calall(real f,real e0,real Dl,real Mc,real eta=0.25,real th=0,real phi=0,real psi=0,real io=0,real be=0,real tc=0,real phic=0, real thi=0,real phii=0,real psii=0);

	//(f,e0,Dl,Mc,eta,tc,phic,th,phi,psi,io,be)
	EPC(real f0_) : f0(f0_)  { };
	EPC() : f0(20.0)  { };

	// arXiv:/0903.0338  (Table.1)
	// Valid from 20-1000 Hz
	real PSD_aLIGO(real f) {
	    real x = f / 215.0;
	    return 1.e-49 * (pow(x,-4.14) - 5.0/(x*x) +  111.0*(1.0 - x*x + 0.5*x*x*x*x) / (1 + 0.5*x*x) );
	}
};
#endif
